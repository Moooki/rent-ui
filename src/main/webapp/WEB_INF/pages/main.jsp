<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
<head>
    <title>Добавление метки с собственным изображением</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

	<script src="/WEB_INF/pages/jquery-3.6.0.min.js" type="text/javascript"></script>
	<script src="/WEB_INF/pages/rest.js" type="text/javascript"></script>
    <script src="/WEB_INF/pages/main.js" type="text/javascript"></script>

	<style>
        html, body, #map {
            width: 100%; height: 100%; padding: 0; margin: 0;
        }
    </style>
</head>
<body>
	<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
	  <div id="toast" class="toast hide text-white bg-danger" role="alert" aria-live="assertive" aria-atomic="true">
		<div class="toast-header">
		  <strong class="me-auto">Ошибка</strong>
		  <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
		</div>
		<div class="toast-body">
		  Ошибка
		</div>
	  </div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="container-fluid">
		<a class="navbar-brand" href="#">iRent</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		  <ul class="navbar-nav me-auto mb-2 mb-lg-0">
		    <li id="link_trips" class="nav-item">
			  <a class="nav-link active" href="#trips-container">Аренды</a>
			</li>
			<li id="link_vehicles" class="nav-item">
			  <a class="nav-link" href="#vehicles-container">Транспорт</a>
			</li>
			<li id="link_parkings" class="nav-item">
			  <a class="nav-link" href="#parkings-container">Парковки</a>
			</li>
		  </ul>		  
		</div>
		<form class="d-flex" action="/rent-ui/logout" method="post">
			<p class="form-control me-2">
				<span id="user-login"></span>
				<span id="user-balance"></span>
			</p>
			<button id="logout" class="btn btn-outline-success" type="submit">Выход</button>
		</form>
	  </div>
	</nav>
	<div id="parkings-container">
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">ID</th>
			  <th scope="col">Наименование</th>
			  <th scope="col">Геопозиция</th>
			  <th scope="col">Радиус, м</th>
			</tr>
		  </thead>
		  <tbody></tbody>
		</table>	
	</div>
	<div id="vehicles-container">
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">ID</th>
			  <th scope="col">Статус</th>
			  <th scope="col">Тип</th>
			  <th scope="col">Парковка</th>
			  <th scope="col">Геопозиция</th>
			</tr>
		  </thead>
		  <tbody></tbody>
		</table>
	</div>
	<div id="trips-container">
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">ID</th>
			  <th scope="col">Клиент</th>
			  <th scope="col">ТС</th>
			  <th scope="col">Статус</th>
			  <th scope="col">Время</th>
			  <th scope="col">Парковки</th>
			  <th scope="col">Цена</th>
			</tr>
		  </thead>
		  <tbody></tbody>
		</table>
	</div>
</body>
</html>