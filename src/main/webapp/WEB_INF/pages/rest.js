var jwtToken = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI1ZDhiOGMxOS1mY2VmLTRiM2UtYWI3MS0yYzhhNzcyZmE3MjciLCJyb2xlIjoiQ0xJRU5UIiwibG9naW4iOiJkbWl0cnkiLCJleHAiOjE2MzkwNTMxMDcsImlhdCI6MTYzOTA0OTUwN30.dnmqU-CydR0HGwvVeOd_xYa1iqVF09WVltZi9kuhwB0mS2lDv8xccf2XasB427PHAjYiCKoH15SYEx5Z_CjpZQ';
var irentServiceUrl = 'http://localhost:8081';


class UserService {	
  user;

  constructor() {
  }	
	
  loadCurrentUser(successMethod) {
    this.getJwt(jwt => {
		jwtToken = jwt;
		callRestService('/users', currentUser => {
			this.user = currentUser;
			successMethod.call();
		});
	});
  }
  
  getJwt(successMethod) {
    $.ajax({
		url: `/jwt`, // вызов сервлета
		success: successMethod,
		error: showErrorToast
	});
  }
}

class ParkingsService {
  constructor() {
  }	
	
  getParkings(successMethod) {
    callRestService('/parking', successMethod);
  }
}

class VehiclesService {
  constructor() {
  }
	
  getVehicles(successMethod) {
    callRestService('/vehicle', successMethod);
  }
} 

class TripsService {
  constructor() {
  }
	
  getTrips(successMethod) {
    callRestService('/rent', successMethod, 'POST', '{}');
  }
}

function callRestService(restUrl, successMethod, method = 'GET',data = null) {
	$.ajax({
		url: `${irentServiceUrl}${restUrl}`,
		method: method,
		data: data,
		headers: {
			"Authorization": `Bearer ${jwtToken}`,
			"Content-Type": 'application/json'
		},
		success: successMethod,
		error: showErrorToast
	});
}

function showErrorToast(error) {
	var errorBody = error.responseJSON;	
	var errorMessage = errorBody.message ? errorBody.message : 'Внутренняя ошибка сервиса';
	
	$('#toast .toast-body').html(errorMessage);
	$('#toast').toast('show');
}
