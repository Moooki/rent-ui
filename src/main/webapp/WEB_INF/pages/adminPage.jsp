<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
</head>
<body>
<div align="left">
    <h3>Admin</h3></br>
    <ul th:object="${user}">
        <li>Id: <span th:text="*{id}"></span></li>
        <li>Login: <span th:text="*{login}"></span></li>
    </ul>
</div>
</body>
</html>