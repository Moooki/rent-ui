var pages = [];//mapContainer, parkingsContainer, vehiclesContainer, tripsContainer;
var activePage;

var parkingsService = new ParkingsService();
var vehiclesService = new VehiclesService();
var tripsService = new TripsService();
var userService = new UserService();

$(document).ready(function() {
	pages = [$("div#trips-container"), $("div#vehicles-container"), $("div#parkings-container")];
	pages.forEach(page => page.hide());
	
	userService.loadCurrentUser(currentUser => {
		$("span#user-login").html(userService.user.login);
		if(userService.user.balance) {
			$("span#user-balance").html(`${userService.user.balance}р.`);
		}
		initLayout();
	});
});

function isAdmin() {
	return user.role == 'ADMIN';
}

function initLayout() {
	activePage = pages[0];
	activePage.show();
    initPage('#trips-container');
	
	$("ul.navbar-nav a").click(function(target) {	
		let container = $("div" + target.currentTarget.hash);
		if(!container.is(activePage)) {
			container.show();
			activePage.hide();
			activePage = container;
			initPage(target.currentTarget.hash);
			$('ul.navbar-nav a').removeClass('active');
			$(this).addClass('active');
		}
	});
}

function initPage(pageName) {
	if(pageName == "#parkings-container") {
		initParkingsPage();
	} else if(pageName == "#vehicles-container") {
		initVehiclesPage();
	} else if(pageName == "#trips-container") {
		initTripsPage();
	}	
}

function initParkingsPage() {
	var tableBody = $('div#parkings-container table tbody');
	parkingsService.getParkings(function(parkings) {
		tableBody.empty();
		parkings.forEach(parking => {
			tableBody.append(
				'<tr>' + 
					`<td>${parking.id}</td>` + 
					`<td>${parking.name}</td>` + 
					`<td>${parking.latitude} ${parking.longitude}</td>` +
					`<td>${parking.radius}</td>` +
				'</tr>');
		});
	});
}

function initVehiclesPage() {
	var tableBody = $('div#vehicles-container table tbody');
	vehiclesService.getVehicles(function(vehicles) {
		tableBody.empty();
		vehicles.forEach(vehicle => {
			tableBody.append(
				'<tr>' + 
					`<td>${vehicle.id}</td>` + 
					`<td>${vehicle.status}</td>` + 
					`<td>${vehicle.type}</td>` + 
					`<td>${vehicle.parking.name}</td>` + 
					`<td>${vehicle.parking.latitude} ${vehicle.parking.longitude}</td>` +
				'</tr>');
		});
	});	
}

function initTripsPage() {
	var tableBody = $('div#trips-container table tbody');
	tripsService.getTrips(function(trips) {
		tableBody.empty();
		trips.forEach(item => {
			tableBody.append(
				'<tr>' + 
					`<td>${item.id}</td>` + 
					`<td>${item.user.login}</td>` + 
					`<td>${item.vehicle.code}</td>` +
					`<td>${item.status}</td>` + 
					`<td>${item.sendTime} - ${item.arrivalTime}</td>` +
					`<td>${item.sendParking.name} -> ${item.arrivalParking.name}</td>` +
					`<td>${item.cost}</td>` +
					
				'</tr>');
		});
	});	
}