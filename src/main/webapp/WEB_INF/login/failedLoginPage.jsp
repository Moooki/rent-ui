<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<div align="center">
    <h2>Failed Login</h2></br>
    <form th:action="@{/login}" th:method="POST" th:object="${user}">
        <label for="login">Login: </label><input id="login" th:field="*{login}" type="text"></br>
        <label for="password">Password: </label><input id="password" th:field="*{password}" type="password"></br>
        <label for="confirm">Confirm: </label><input id="confirm" name="confirmPassword" type="password"></br>
        <button type="submit" style="width: 50px">Login</button>
    </form></br>
</div>
</body>
</html>