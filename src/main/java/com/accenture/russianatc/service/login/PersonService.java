package com.accenture.russianatc.service.login;

import com.accenture.russianatc.access.PersonDAO;
import com.accenture.russianatc.model.dto.AdminDTO;
import com.accenture.russianatc.model.dto.PersonDTO;
import com.accenture.russianatc.model.dto.UserDTO;
import com.accenture.russianatc.model.entity.user.Admin;
import com.accenture.russianatc.model.entity.user.Person;
import com.accenture.russianatc.model.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;

public class PersonService {

    @Autowired
    private PersonDAO personDAO;

    public PersonDTO getUser(String login) {
        Person person = personDAO.setPerson(login);
        if(person instanceof User) {
            return UserDTO.modelToDTO((User) person);
        } else {
            return AdminDTO.modelToDTO((Admin) person);
        }
    }
}
