package com.accenture.russianatc.service.login;

import com.accenture.russianatc.access.UserDAO;
import com.accenture.russianatc.model.exception.NonExistentLoginException;
import com.accenture.russianatc.model.exception.WrongLoginException;
import com.accenture.russianatc.model.exception.WrongPasswordException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Класс входа в систему
 */
@Slf4j
public class LoginService {

    private static final String regex = "(\\w+|\\d+|\\_)";

    @Autowired
    private UserDAO userDAO;

    /**
     * Проверяет полученные данные на соответствие требованиям
     * Отправляет классу LoginSystemDao логин для поиска по базе данных
     * Если пользователь существует, то сравниваем введённый пароль с полученным от LoginSystemDao
     * @param login Логин (Почта) пользователя
     * @param password Пароль пользователя
     * @param confirmPassword Повторно введённый пароль
     */
    public boolean login(String login, String password, String confirmPassword) {
        try {
            if (login.length() > 20
                    || login.isEmpty()
                    || !login.toLowerCase().matches(regex)) {
                throw new WrongLoginException();
            }
            if (password.length() > 20
                    || password.isEmpty()
                    || !password.toLowerCase().matches(regex)) {
                throw new WrongPasswordException();
            } else if(!password.equals(confirmPassword)) {
                throw new WrongPasswordException();
            }
        } catch (WrongLoginException | WrongPasswordException e) {
            log.info(e.getMessage());
            return false;
        }

        try {
            String realPassword = userDAO.findUser(login);
            if(realPassword.equals(password)) {
                return true;
            } else {
                log.info("Введён неверный пароль");
                return false;
            }
        } catch (NonExistentLoginException e) {
            log.error("Такой пользователь не найден", e);
            return false;
        }
    }
}
