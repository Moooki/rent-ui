package com.accenture.russianatc.model.dto;

import com.accenture.russianatc.model.entity.user.Admin;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Data
public class AdminDTO extends PersonDTO {

    public static AdminDTO modelToDTO(Admin admin) {
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setId(admin.getId());
        adminDTO.setLogin(admin.getLogin());
        adminDTO.setGroup(admin.getGroup());
        return adminDTO;
    }
}
