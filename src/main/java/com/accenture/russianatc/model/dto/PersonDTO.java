package com.accenture.russianatc.model.dto;

import com.accenture.russianatc.model.entity.user.PersonGroup;
import lombok.Data;

@Data
public abstract class PersonDTO {

    private Long id;
    private String login;
    private PersonGroup group;
}
