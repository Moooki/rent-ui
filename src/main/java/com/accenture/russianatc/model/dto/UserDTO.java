package com.accenture.russianatc.model.dto;

import com.accenture.russianatc.model.entity.user.User;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Data
public class UserDTO extends PersonDTO {

    private BigDecimal balance;

    public static UserDTO modelToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setGroup(user.getGroup());
        userDTO.setBalance(user.getBalance());
        return userDTO;
    }
}
