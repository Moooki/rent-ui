package com.accenture.russianatc.model.exception;

/**
 * Класс исключения некорректно введённого пароля
 */
public class WrongPasswordException extends Exception {
    public WrongPasswordException() {
        super("Неправильный формат ввода пароля");
    }
}