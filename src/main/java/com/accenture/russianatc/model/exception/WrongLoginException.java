package com.accenture.russianatc.model.exception;

/**
 * Класс исключения некорректно введённого логина
 */
public class WrongLoginException extends Exception {
    public WrongLoginException() {
        super("Неправильный формат ввода логина");
    }
}