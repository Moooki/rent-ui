package com.accenture.russianatc.model.exception;

public class NonExistentLoginException extends Exception {
    public NonExistentLoginException() {
        super("Указанный логин не существует");
    }
}
