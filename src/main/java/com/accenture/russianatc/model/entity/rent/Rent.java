package com.accenture.russianatc.model.entity.rent;

import com.accenture.russianatc.model.entity.parking.Point;
import com.accenture.russianatc.model.entity.transport.Vehicle;
import lombok.Data;

/**
 * Представляет аренду транспортного средаства
 */
@Data
public class Rent {

    private long id;
    private Vehicle vehicle;
    private TimeRent sendingTime, arrivalTime;
    private Point sendingPoint, arrivalPoint;
    private StatusRent currentState;

    @Override
    public String toString() {
        return "Rent" + "\n" +
                " ID: " + id + "\n" +
                " Vehicle: " + vehicle + "\n" +
                " Send time: " + sendingTime + "\n" +
                " Arrival time: " + arrivalTime + "\n" +
                " Send point: " + sendingPoint + "\n" +
                " Arrival point: " + arrivalPoint + "\n" +
                " Status: " + currentState;
    }
}
