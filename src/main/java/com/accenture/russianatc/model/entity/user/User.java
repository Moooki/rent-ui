package com.accenture.russianatc.model.entity.user;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Представляет арендатора
 */
public class User extends Person {

    @Getter
    @Setter
    private BigDecimal balance;

    @Override
    public PersonGroup getGroup() { return super.getGroup(); }

    @Override
    public String toString() {
        return "User" + "\n" +
                " ID: " + getId() + "\n" +
                " Login: " + getLogin() + "\n" +
                " Balance: " + balance;
    }
}
