package com.accenture.russianatc.model.entity.transport;

/**
 * Представляет электросамокат
 */
public class ElectricScooter extends Vehicle {

    private static final VehicleType type = VehicleType.ElectricScooter;

    private byte charge;
    private byte maxSpeed;

    @Override
    public String toString() {
        return super.toString() + " Type: " + type + "\n" +
                " Charge: " + charge + "\n" +
                " Max speed: " + maxSpeed;
    }
}
