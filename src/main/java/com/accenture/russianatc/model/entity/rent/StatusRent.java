package com.accenture.russianatc.model.entity.rent;

/**
 * Представляет статус аренды (Создана, Завершена)
 */
public enum StatusRent {
    Created("Created"), Completed("Completed");

    private final String name;

    StatusRent(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
