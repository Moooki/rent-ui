package com.accenture.russianatc.model.entity.transport;

/**
 * Состояние транспортного средства
 */
public enum StatusVehicle {
    Excellent("Excellent"), Well("Well"), Satisfactory("Satisfactory");

    private final String name;

    StatusVehicle(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
