package com.accenture.russianatc.model.entity.user;

import lombok.Data;

@Data
public class Person {

    private long id;
    private String login;
    private String password;
    private PersonGroup group;
}
