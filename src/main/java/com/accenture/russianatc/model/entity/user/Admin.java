package com.accenture.russianatc.model.entity.user;

/**
 * Представляет администратора
 */
public class Admin extends Person {

    @Override
    public PersonGroup getGroup() {
        return super.getGroup();
    }

    @Override
    public String toString() {
        return "Admin" + "\n" +
                " ID: " + getId() + "\n" +
                " Login: " + getLogin();
    }
}
