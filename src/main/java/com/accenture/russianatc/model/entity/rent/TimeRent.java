package com.accenture.russianatc.model.entity.rent;

/**
 * Представляет время аренды
 */
public class TimeRent {
    private final byte hour;
    private final byte minute;
    private final byte second;

    private TimeRent(byte hour, byte minute, byte second){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    @Override
    public String toString() {
        return hour + ":" + minute + ":" + second;
    }
}
