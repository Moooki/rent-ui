package com.accenture.russianatc.security;

import com.accenture.russianatc.model.dto.PersonDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtBuilder {

    @Value("${jwt.secret}")
    private String secret;

    public String getToken(PersonDTO personDTO) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("login", personDTO.getLogin());
        claims.put("group", personDTO.getGroup());

        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + 3_600_000);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(personDTO.getId().toString())
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }
}
