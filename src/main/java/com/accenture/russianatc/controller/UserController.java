package com.accenture.russianatc.controller;

import com.accenture.russianatc.model.dto.AdminDTO;
import com.accenture.russianatc.model.dto.PersonDTO;
import com.accenture.russianatc.model.entity.user.Admin;
import com.accenture.russianatc.model.entity.user.Person;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@Scope("session")
@SessionAttributes("user")
@RequestMapping("/user")
@Slf4j
public class UserController {

    @GetMapping
    public String showUserInfo(HttpSession session, HttpServletResponse response) {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        PersonDTO user = (PersonDTO) session.getAttribute("user");
        if (user != null) {
            if (user instanceof AdminDTO) {
                log.warn("Попытка несанкционированного доступа к странице пользователя");
                return "redirect:/admin";
            } else {
                return "/pages/main";
            }
        }
        log.warn("Попытка несанкционированного доступа к странице пользователя");
        return "redirect:/login";
    }
}
