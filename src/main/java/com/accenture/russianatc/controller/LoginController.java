package com.accenture.russianatc.controller;

import com.accenture.russianatc.model.dto.PersonDTO;
import com.accenture.russianatc.model.entity.user.Person;
import com.accenture.russianatc.security.JwtBuilder;
import com.accenture.russianatc.service.login.LoginService;
import com.accenture.russianatc.service.login.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@Scope("session")
@SessionAttributes("user")
@RequestMapping("/login")
@Slf4j
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private JwtBuilder jwtBuilder;

    @Autowired
    private PersonService personService;

    @GetMapping
    public String getLoginPage() {
        return "/login/loginPage";
    }

    @PostMapping
    public String loginSystem(
            @RequestParam("login") String login,
            @RequestParam("password") String password,
            @RequestParam("confirmPassword") String confirmPassword,
            HttpSession session, HttpServletResponse response) {
        if(loginService.login(login, password, confirmPassword)) {
            PersonDTO personDTO = personService.getUser(login);
            session.setAttribute("user", personDTO);
            log.debug("Успешный вход");
            String token = jwtBuilder.getToken(personDTO);
            log.info("Сгенерирован токен: " + token);
            response.setHeader(HttpHeaders.AUTHORIZATION, token);
            return "redirect:/user";
        }
        log.info("Неуспешный вход");
        return "/login/failedLoginPage";
    }
}
