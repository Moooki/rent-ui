package com.accenture.russianatc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@Scope("session")
@SessionAttributes("user")
@RequestMapping("/logout")
@Slf4j
public class LogoutController {

    @PostMapping
    public String showUserInfo(SessionStatus status) {
        status.setComplete();
        log.info("Пользователь вышел из системы");
        return "redirect:/login";
    }
}
