package com.accenture.russianatc.configuration;

import com.accenture.russianatc.model.entity.user.Admin;
import com.accenture.russianatc.model.entity.user.Person;
import com.accenture.russianatc.model.entity.user.User;
import com.accenture.russianatc.service.login.LoginService;
import com.accenture.russianatc.service.login.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@ComponentScan("com.accenture.russianatc")
@PropertySource("classpath:application.properties")
@EnableWebMvc
public class ProjectConfiguration implements WebMvcConfigurer {

    private final ApplicationContext applicationContext;

    @Autowired
    public ProjectConfiguration(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public InternalResourceViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB_INF");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    @Scope("session")
    public User getUser() {
        return new User();
    }

    @Bean
    @Scope("session")
    public Admin getAdmin() {
        return new Admin();
    }

    @Bean
    @Scope("session")
    public Person getPerson() {
        return new Person();
    }

    @Bean
    @Scope("singleton")
    public LoginService getLoginService() {
        return new LoginService();
    }

    @Bean
    @Scope("singleton")
    public PersonService getPersonService() {
        return new PersonService();
    }

}
