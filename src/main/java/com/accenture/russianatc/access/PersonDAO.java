package com.accenture.russianatc.access;

import com.accenture.russianatc.model.entity.user.Admin;
import com.accenture.russianatc.model.entity.user.Person;
import com.accenture.russianatc.model.entity.user.PersonGroup;
import com.accenture.russianatc.model.entity.user.User;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Component
@Scope("singleton")
@Slf4j
public class PersonDAO {

    @Autowired
    private DBConnectionManager dbConnectionManager;

    /**
     * Собирает объект Person
     * @param login и password
     * @return Собранный Person
     */
    public Person setPerson(String login) {
        Connection connection = null;
        Statement state = null;
        ResultSet result = null;

        try {
            String sql = String.format("SELECT * FROM users WHERE login = '%s';", login);
            connection = dbConnectionManager.getConnection();

            if(connection == null) {
                throw new SQLException("Пустой connection");
            }

            state = connection.createStatement();
            result = state.executeQuery(sql);

            if(result.next()) {
                switch (result.getString("user_group")) {
                    case "USER":
                        User user = new User();
                        user.setGroup(PersonGroup.USER);
                        user.setId(result.getLong("id"));
                        user.setLogin(login);
                        user.setBalance(result.getBigDecimal("balance"));
                        user.setPassword(result.getString("password"));
                        return user;
                    case "ADMIN":
                        Admin admin = new Admin();
                        admin.setGroup(PersonGroup.ADMIN);
                        admin.setId(result.getLong("id"));
                        admin.setLogin(login);
                        admin.setPassword(result.getString("password"));
                        return admin;
                }
            }
            return null;

        } catch (SQLException e) {
            log.error("Ошибка в запросе SQL", e);
        } finally {
            try {
                dbConnectionManager.closeConnection(connection);
                if (state != null) state.close();
                if (result != null) result.close();
            } catch (SQLException e) {
                log.error("Не удалось закрыть ресурсы", e);
            }
        }
        return null;
    }
}
