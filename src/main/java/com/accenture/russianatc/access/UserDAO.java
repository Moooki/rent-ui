package com.accenture.russianatc.access;

import com.accenture.russianatc.model.exception.NonExistentLoginException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Выполняет обращение к базе данных пользователей
 */
@Component
@Scope("singleton")
@Slf4j
public class UserDAO {

    @Autowired
    private DBConnectionManager dbConnectionManager;

    /**
     * Ищет пользователя в базе данных
     * @param login ищет по логину
     * @return если нашли такой логин, то вернём пароль для проверки
     * @throws NonExistentLoginException не существующий логин
     */
    public String findUser(String login) throws NonExistentLoginException {
        Connection connection = null;
        Statement state = null;
        ResultSet result = null;

        try {
            String sql = String.format("SELECT password FROM users WHERE login = '%s';", login);
            connection = dbConnectionManager.getConnection();

            if(connection == null) {
                throw new SQLException("Пустой connection");
            }

            state = connection.createStatement();
            result = state.executeQuery(sql);
            if (result.next()) {
                return result.getString("password");
            } else {
                throw new NonExistentLoginException();
            }
        } catch (SQLException e) {
            log.error("Ошибка в запросе SQL", e);
        } finally {
            try {
                dbConnectionManager.closeConnection(connection);
                if (state != null) state.close();
                if (result != null) result.close();
            } catch (SQLException e) {
                log.error("Не удалось закрыть ресурсы", e);
            }
        }
        return null;
    }
}
