package com.accenture.russianatc.access;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
@Scope("prototype")
@Slf4j
public class DBConnectionManager {

    @Value("${db.url}")
    private String url;

    @Value("${db.login}")
    private String login;

    @Value("${db.password}")
    private String password;

    public Connection getConnection() {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection(url, login, password);
        } catch (SQLException e) {
            log.error("Не удалось подключиться к базе данных", e);
        } catch (ClassNotFoundException e) {
            log.error("Не удалось загрузить драйвер", e);
        }
        return null;
    }

    public void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("Не удалось закрыть соединение с базой данных", e);
        }
    }
}
